python -u main.py --prefix indian_pines --cuda True --epochs 1000 --samples 15 --num_classes 16 > ind1
python -u main.py --prefix indian_pines --cuda True --epochs 1000 --samples 15 --num_classes 16 > ind2
python -u main.py --prefix indian_pines --cuda True --epochs 1000 --samples 15 --num_classes 16 > ind3
python -u main.py --prefix indian_pines --cuda True --epochs 1000 --samples 15 --num_classes 16 > ind4
python -u main.py --prefix indian_pines --cuda True --epochs 1000 --samples 15 --num_classes 16 > ind5
python -u main.py --prefix indian_pines --cuda True --epochs 1000 --samples 15 --num_classes 16 > ind6
python -u main.py --prefix indian_pines --cuda True --epochs 1000 --samples 15 --num_classes 16 > ind7
python -u main.py --prefix indian_pines --cuda True --epochs 1000 --samples 15 --num_classes 16 > ind8
python -u main.py --prefix indian_pines --cuda True --epochs 1000 --samples 15 --num_classes 16 > ind9
python -u main.py --prefix indian_pines --cuda True --epochs 1000 --samples 15 --num_classes 16 > ind10


python -u main.py --prefix indian_pines_corrected --cuda True --epochs 1000 --samples 15 --num_classes 16 > ind1
python -u main.py --prefix indian_pines_corrected --cuda True --epochs 1000 --samples 15 --num_classes 16 > ind2
python -u main.py --prefix indian_pines_corrected --cuda True --epochs 1000 --samples 15 --num_classes 16 > ind3
python -u main.py --prefix indian_pines_corrected --cuda True --epochs 1000 --samples 15 --num_classes 16 > ind4
python -u main.py --prefix indian_pines_corrected --cuda True --epochs 1000 --samples 15 --num_classes 16 > ind5
python -u main.py --prefix indian_pines_corrected --cuda True --epochs 1000 --samples 15 --num_classes 16 > ind6
python -u main.py --prefix indian_pines_corrected --cuda True --epochs 1000 --samples 15 --num_classes 16 > ind7
python -u main.py --prefix indian_pines_corrected --cuda True --epochs 1000 --samples 15 --num_classes 16 > ind8
python -u main.py --prefix indian_pines_corrected --cuda True --epochs 1000 --samples 15 --num_classes 16 > ind9
python -u main.py --prefix indian_pines_corrected --cuda True --epochs 1000 --samples 15 --num_classes 16 > ind10


python -u main.py --prefix paviau --cuda True --epochs 1000 --samples 15 --num_classes 9 --input_size 1 > paviau1
python -u main.py --prefix paviau --cuda True --epochs 1000 --samples 15 --num_classes 9 > paviau2
python -u main.py --prefix paviau --cuda True --epochs 1000 --samples 15 --num_classes 9 > paviau3
python -u main.py --prefix paviau --cuda True --epochs 1000 --samples 15 --num_classes 9 > paviau4
python -u main.py --prefix paviau --cuda True --epochs 1000 --samples 15 --num_classes 9 > paviau5
python -u main.py --prefix paviau --cuda True --epochs 1000 --samples 15 --num_classes 9 > paviau6
python -u main.py --prefix paviau --cuda True --epochs 1000 --samples 15 --num_classes 9 > paviau7
python -u main.py --prefix paviau --cuda True --epochs 1000 --samples 15 --num_classes 9 > paviau8
python -u main.py --prefix paviau --cuda True --epochs 1000 --samples 15 --num_classes 9 > paviau9
python -u main.py --prefix paviau --cuda True --epochs 1000 --samples 15 --num_classes 9 > paviau10


python -u main.py --prefix paviac --cuda True --epochs 1000 --samples 15  --num_classes 9 > paviac1
python -u main.py --prefix paviac --cuda True --epochs 1000 --samples 15  --num_classes 9 > paviac2
python -u main.py --prefix paviac --cuda True --epochs 1000 --samples 15  --num_classes 9 > paviac3
python -u main.py --prefix paviac --cuda True --epochs 1000 --samples 15  --num_classes 9 > paviac4
python -u main.py --prefix paviac --cuda True --epochs 1000 --samples 15  --num_classes 9 > paviac5
python -u main.py --prefix paviac --cuda True --epochs 1000 --samples 15  --num_classes 9 > paviac6
python -u main.py --prefix paviac --cuda True --epochs 1000 --samples 15  --num_classes 9 > paviac7
python -u main.py --prefix paviac --cuda True --epochs 1000 --samples 15  --num_classes 9 > paviac8
python -u main.py --prefix paviac --cuda True --epochs 1000 --samples 15  --num_classes 9 > paviac9
python -u main.py --prefix paviac --cuda True --epochs 1000 --samples 15  --num_classes 9 > paviac10



python -u main.py --prefix ksc --cuda True --epochs 1000 --samples 15 --num_classes 13 > ksc1
python -u main.py --prefix ksc --cuda True --epochs 1000 --samples 15 --num_classes 13 > ksc2
python -u main.py --prefix ksc --cuda True --epochs 1000 --samples 15 --num_classes 13 > ksc3
python -u main.py --prefix ksc --cuda True --epochs 1000 --samples 15 --num_classes 13 > ksc4
python -u main.py --prefix ksc --cuda True --epochs 1000 --samples 15 --num_classes 13 > ksc5
python -u main.py --prefix ksc --cuda True --epochs 1000 --samples 15 --num_classes 13 > ksc6
python -u main.py --prefix ksc --cuda True --epochs 1000 --samples 15 --num_classes 13 > ksc7
python -u main.py --prefix ksc --cuda True --epochs 1000 --samples 15 --num_classes 13 > ksc8
python -u main.py --prefix ksc --cuda True --epochs 1000 --samples 15 --num_classes 13 > ksc9
python -u main.py --prefix ksc --cuda True --epochs 1000 --samples 15 --num_classes 13 > ksc10



python -u main.py --prefix botswana --cuda True --epochs 1000 --samples 15 --num_classes 14 > botswana1
python -u main.py --prefix botswana --cuda True --epochs 1000 --samples 15 --num_classes 14 > botswana2
python -u main.py --prefix botswana --cuda True --epochs 1000 --samples 15 --num_classes 14 > botswana3
python -u main.py --prefix botswana --cuda True --epochs 1000 --samples 15 --num_classes 14 > botswana4
python -u main.py --prefix botswana --cuda True --epochs 1000 --samples 15 --num_classes 14 > botswana5
python -u main.py --prefix botswana --cuda True --epochs 1000 --samples 15 --num_classes 14 > botswana6
python -u main.py --prefix botswana --cuda True --epochs 1000 --samples 15 --num_classes 14 > botswana7
python -u main.py --prefix botswana --cuda True --epochs 1000 --samples 15 --num_classes 14 > botswana8
python -u main.py --prefix botswana --cuda True --epochs 1000 --samples 15 --num_classes 14 > botswana9
python -u main.py --prefix botswana --cuda True --epochs 1000 --samples 15 --num_classes 14 > botswana10



python -u main.py --prefix salinas --cuda True --epochs 1000 --samples 15 --num_classes 16 > salinas1
python -u main.py --prefix salinas --cuda True --epochs 1000 --samples 15 --num_classes 16 > salinas2
python -u main.py --prefix salinas --cuda True --epochs 1000 --samples 15 --num_classes 16 > salinas3
python -u main.py --prefix salinas --cuda True --epochs 1000 --samples 15 --num_classes 16 > salinas4
python -u main.py --prefix salinas --cuda True --epochs 1000 --samples 15 --num_classes 16 > salinas5
python -u main.py --prefix salinas --cuda True --epochs 1000 --samples 15 --num_classes 16 > salinas6
python -u main.py --prefix salinas --cuda True --epochs 1000 --samples 15 --num_classes 16 > salinas7
python -u main.py --prefix salinas --cuda True --epochs 1000 --samples 15 --num_classes 16 > salinas8
python -u main.py --prefix salinas --cuda True --epochs 1000 --samples 15 --num_classes 16 > salinas9
python -u main.py --prefix salinas --cuda True --epochs 1000 --samples 15 --num_classes 16 > salinas10