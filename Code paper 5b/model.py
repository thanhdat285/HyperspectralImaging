import torch
import torch.nn as nn
# import torch.nn.functional as F
import pdb

class Model(nn.Module):
    def __init__(self, input_size, num_channels, num_classes):
        super(Model, self).__init__()
        self.input_size = input_size
        self.num_channels = num_channels
        self.num_classes = num_classes
        self._init_model()
        self.loss_fn = nn.CrossEntropyLoss()

    def _init_model(self):
        conv1 = nn.Conv2d(in_channels=self.num_channels, out_channels=128, kernel_size=1, padding=0)
        conv2 = nn.Conv2d(in_channels=128, out_channels=64, kernel_size=1, padding=0)
        conv3 = nn.Conv2d(in_channels=64, out_channels=self.num_classes, kernel_size=1, padding=0)
        nn.init.normal_(conv1.weight, std=0.05)
        nn.init.normal_(conv2.weight, std=0.05)
        nn.init.normal_(conv3.weight, std=0.05)
        conv2.bias = torch.nn.Parameter(torch.zeros(conv2.bias.shape))
        conv3.bias = torch.nn.Parameter(torch.zeros(conv3.bias.shape))

        self.model = nn.Sequential(
            conv1,
            nn.ReLU(),
            nn.LocalResponseNorm(size=3, alpha=0.0001, beta=0.75),
            nn.Dropout(0.6),
            conv2,
            nn.ReLU(),
            nn.LocalResponseNorm(size=3, alpha=0.0001, beta=0.75),
            nn.Dropout(0.6),
            conv3,
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=self.input_size)
        )


    def forward(self, batch_hsi):
        """
        batch_hsi: torch.FloatTensor()
        """
        scores = self.model(batch_hsi)
        N, C, _, _ = scores.shape
        scores = scores.view(N, C)
        return scores

    def loss(self, batch_hsi, gts):
        scores = self.forward(batch_hsi)
        return self.loss_fn(scores, gts)
