import argparse
import scipy.io
import numpy as np
import time

import torch
import torch.nn as nn

from model import Model
import spectral.io.envi as envi
from spectral import *
import re
from scipy import signal
import pdb

paviau = [
['asphalt', 'asphalt_shingle gds367 dkgry',    'usgs/manmade_asd_2151.hdr'],
['asphalt', 'asphalt_shingle gds368 lgray',    'usgs/manmade_asd_2151.hdr'],
['asphalt', 'reddish asphalt shingle', 'aster/manmade_jhu_becknic_536.hdr'],
['asphalt', 'asphalt gds376 blck_road old',    'usgs/manmade_asd_2151.hdr'],
['asphalt', 'asphalt_shingle gds366 tan',  'usgs/manmade_asd_2151.hdr'],
['asphalt', 'asphalt_tar gds346 blck_roof',    'usgs/manmade_asd_2151.hdr'],
['asphalt', 'construction asphalt',    'aster/manmade_jhu_becknic_491.hdr,aster/manmade_jhu_becknic_491.hdr,aster/manmade_jhu_becknic_536.hdr'],
['asphalt', 'reddish asphalt roofing shingle', 'aster/manmade_jhu_becknic_536.hdr'],
['asphalt', 'asphalt shingle', 'aster/manmade_jhu_becknic_561.hdr'],
['asphalt', 'asphalt roofing shingle', 'aster/manmade_jhu_becknic_561.hdr'],
['gravel',   'jasper ridge gravel',  'veg_lib/veg_2grn.hdr'],
['painted metal sheets', 'oxidized galvanized steel metal',  'aster/manmade_jhu_becknic_536.hdr'],
['painted metal sheets', 'aluminum metal',   'aster/manmade_jhu_becknic_536.hdr'],
['painted metal sheets', 'galvanized steel metal',   'aster/manmade_jhu_becknic_536.hdr,aster/manmade_jhu_becknic_536.hdr'],
['painted metal sheets', 'sheet_metal gds352 crg galvn', 'usgs/manmade_asd_2151.hdr'],
['painted metal sheets', 'copper metal', 'aster/manmade_jhu_becknic_536.hdr,aster/manmade_jhu_becknic_536.hdr,aster/manmade_jhu_becknic_536.hdr'],
['painted metal sheets', 'painted_aluminum gds333 lggr', 'usgs/manmade_asd_2151.hdr'],
['bare soil',    'grey calcareous silty soil',   'aster/soil_jpl_nicolet_2223.hdr,aster/soil_jpl_nicolet_2223.hdr'],
['bare soil',    'red-orange sandy soil',    'aster/soil_jpl_nicolet_2223.hdr'],
['bare soil',    'jasper ridge grassland soil',  'veg_lib/veg_2grn.hdr']
]

paviau_labels = ['asphalt', 'meadows', 'gravel', 'trees', 'painted metal sheets', 'bare soil', 'bitumen', 'self-blocking bricks', 'shadows']
paviau_wavelength = [0.43, 0.86]
paviau_channels = 103



indian_pines = [
['grass-pasture',    'lawn_grass gds91 /shiftd 3nm', 'usgs/vegetation_beckman_3785.hdr,usgs/vegetation_beckman_469.hdr'],
['grass-pasture',    'grass-fescue-wheatg ynp-fw-3', 'usgs/vegetation_aviris_2138.hdr,usgs/vegetation_aviris_217.hdr'],
['grass-pasture',    'grass-fescueneedleg ynp-fn-1', 'usgs/vegetation_aviris_2138.hdr,usgs/vegetation_aviris_217.hdr'],
['grass-pasture',    'grass_golden_dry gds480',  'usgs/vegetation_asd_2151.hdr'],
['grass-pasture',    'grass',    'aster/vegetation_jhu_becknic_550.hdr'],
['grass-pasture',    'grass_dry.6+.4green amx29',    'usgs/vegetation_beckman_3785.hdr,usgs/vegetation_beckman_469.hdr'],
['grass-pasture',    'dry_long_grass av87-2',    'usgs/vegetation_beckman_3785.hdr,usgs/vegetation_beckman_469.hdr'],
['grass-pasture',    'grass_dry.4+.6green amx27',    'usgs/vegetation_beckman_3785.hdr,usgs/vegetation_beckman_469.hdr'],
['grass-pasture',    'grass-fescue-wheatg ynp-fw-2', 'usgs/vegetation_aviris_2138.hdr,usgs/vegetation_aviris_217.hdr'],
['grass-pasture',    'grass-smoothbrome ynp-sb-1',   'usgs/vegetation_aviris_2138.hdr,usgs/vegetation_aviris_217.hdr'],
['grass-pasture',    'dry grass',    'aster/vegetation_jhu_becknic_2559.hdr,veg_lib/veg_2grn.hdr'],
['grass-pasture',    'lawn_grass gds91 (green)', 'usgs/vegetation_beckman_3785.hdr,usgs/vegetation_beckman_469.hdr'],
['grass-pasture',    'grass_dry.5+.5green amx28',    'usgs/vegetation_beckman_3785.hdr,usgs/vegetation_beckman_469.hdr'],
['grass-pasture',    'lawn_grass gds91 +const 1.0',  'usgs/vegetation_beckman_3785.hdr,usgs/vegetation_beckman_469.hdr'],
['grass-pasture',    'lawn_grass gds91 +1shf/unshf', 'usgs/vegetation_beckman_3785.hdr,usgs/vegetation_beckman_469.hdr'],
['grass-pasture',    'grass-fescue-wheatg ynp-fw-1', 'usgs/vegetation_aviris_2138.hdr,usgs/vegetation_aviris_217.hdr'],
['grass-pasture',    'lawn_grass gds91 +1shft 3nm',  'usgs/vegetation_beckman_3785.hdr,usgs/vegetation_beckman_469.hdr'],
['grass-pasture',    'grass_dry.9+.1green amx32',    'usgs/vegetation_beckman_3785.hdr,usgs/vegetation_beckman_469.hdr'],
['grass-pasture',    'grass_dry.8+.2green amx31',    'usgs/vegetation_beckman_3785.hdr,usgs/vegetation_beckman_469.hdr'],
['grass-pasture',    'lawn_grass gds91 shifted 3nm', 'usgs/vegetation_beckman_3785.hdr,usgs/vegetation_beckman_469.hdr'],
['grass-pasture',    'grass_dry.7+.3green amx30',    'usgs/vegetation_beckman_3785.hdr,usgs/vegetation_beckman_469.hdr']
]
indian_pines_label = ['alfalfa', 'corn-notill', 'corn-mintill', 'corn', 'grass-pasture', 'grass-trees', 'grass-pasture-mowed',
'hay-windrowed', 'oats', 'soybean-notill', 'soybean-mintill', 'soybean-clean', 'wheat', 'woods', 'buildings-grass-trees-drives', 'stone-steel-towers']
indian_pines_wavelength = [0.4, 2.5]
indian_pines_channels = 220


def parse_args():
    parser = argparse.ArgumentParser(description="Evaluate data speclib.")
    parser.add_argument('--prefix',     default="indian_pines", help="Which data to evaluate.")
    parser.add_argument('--model', default="model.pt", help="Model to load.")
    parser.add_argument('--speclib_dir', default="../data 2 speclib/", help="Speclib directory.")
    parser.add_argument('--cuda',       default=False, help="Whether use cuda or not.", type=bool)
    return parser.parse_args()

def load_data_speclib(args):
    intersect = eval(args.prefix)
    wavelength = eval("{}_wavelength".format(args.prefix))
    channels = eval("{}_channels".format(args.prefix))
    labels = eval("{}_labels".format(args.prefix))

    spectrals = []
    labels = []
    spectral_names = []
    for elm in intersect:
        paths = elm[2].split(',')
        spectral_name = elm[1]
        for path in paths:
            img = envi.open(args.speclib_dir+"/"+path)
            for idx, name in enumerate(img.names):
                name = re.sub(r"\[.+\]", "", name).strip().lower()
                anchor_idxs = list(map(lambda x: wavelength[0] <=x <=wavelength[-1], img.bands.centers))
                if spectral_name == name:
                    if img.spectra[idx][anchor_idxs].shape[0] == 0: continue
                    s = signal.resample(img.spectra[idx][anchor_idxs], channels)
                    virtual_image = np.zeros((5,5,channels))
                    for xx in range(5):
                        for yy in range(5):
                            virtual_image[xx,yy,:] = s
                    spectrals.append(virtual_image)
                    labels.append(labels.index(elm[0]))
                    spectral_names.append([
                        name,
                        path
                    ])
    spectrals = np.array(spectrals).reshape(-1, channels, 5, 5)
    labels = np.array(labels)
    spectrals = torch.FloatTensor(spectrals)
    labels = torch.LongTensor(labels)
    if args.cuda:
        spectrals = spectrals.cuda()
        labels = labels.cuda()
    return spectrals, labels, spectral_names

def evaluate(model, spectrals, labels, spectral_names):
    print("Evaluating...")
    try:
        scores = model(spectrals)
    except:
        print("Loading spectrals and labels to cuda.")
        spectrals = spectrals.cuda()
        labels = labels.cuda()
        scores = model(spectrals)
    predict_labels = scores.max(dim=1)[1]
    correct = predict_labels == labels
    n_correct = correct.sum()
    print("Correct: {}/{}".format(n_correct, len(labels)))

    with open("results.txt", "w+") as file:
        file.write('Material\tPredicted correct?\tFile\n')
        for idx, spectral in enumerate(spectral_names):
            file.write('\t'.join([spectral[0], str(correct[idx].item()), spectral[1], '\n']))
    print("Result has been written to results.txt")

if __name__ == '__main__':
    args = parse_args()
    print(args)
    model = torch.load(args.model)
    spectrals, labels, spectral_names = load_data_speclib(args)
    evaluate(model, spectrals, labels, spectral_names)
