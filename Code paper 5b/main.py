import argparse
import scipy.io
import numpy as np
import time

import torch
import torch.nn as nn

from model import Model
import pdb

def parse_args():
    parser = argparse.ArgumentParser(description="Code paper 5b.")
    parser.add_argument('--prefix',     default="Indian_pines", help="Which data to train and evaluate.")
    parser.add_argument('--cuda',       default=False, help="Whether use cuda or not.", type=bool)
    parser.add_argument('--input_size', default=5, help="Input size to CNN.", type=int)
    parser.add_argument('--batch_size', default=16, help="Batch size.", type=int)
    parser.add_argument('--epochs',     default=10, help="Number of epochs.", type=int)
    parser.add_argument('--learning_rate', default=0.001, help="Learning rate.", type=float)
    parser.add_argument('--num_classes', default=9, help="Number of classes to classify.", type=int)
    parser.add_argument('--samples',    default=16, help="Number of samples per class", type=int)
    parser.add_argument('--print_every', default=50, help="How often to print training info.", type=int)
    parser.add_argument('--validate_iter', default=20000, help="How often to run validation.", type=int)
    return parser.parse_args()

def data_augmentation(hsi):
    data = [hsi, hsi[:,::-1,:]]
    temp_hsi = hsi
    for i in range(3):
        temp_hsi = np.rot90(temp_hsi, axes=(0,1))
        data.append(temp_hsi)
        data.append(temp_hsi[:,::-1,:])
    return data

def load_indian_pines():
    hsi = scipy.io.loadmat("../data 1/Indian Pines/Indian_pines_corrected.mat")["indian_pines_corrected"]
    gt = scipy.io.loadmat("../data 1/Indian Pines/Indian_pines_gt.mat")["indian_pines_gt"]
    return hsi, gt
def load_botswana():
    hsi = scipy.io.loadmat("../data 1/Botswana/Botswana.mat")["Botswana"]
    gt = scipy.io.loadmat("../data 1/Botswana/Botswana_gt.mat")["Botswana_gt"]
    return hsi, gt
def load_ksc():
    hsi = scipy.io.loadmat("../data 1/KSC/KSC.mat")["KSC"]
    gt = scipy.io.loadmat("../data 1/KSC/KSC_gt.mat")["KSC_gt"]
    return hsi, gt
def load_paviac():
    hsi = scipy.io.loadmat("../data 1/PaviaC/PaviaC.mat")["pavia"]
    gt = scipy.io.loadmat("../data 1/PaviaC/PaviaC_gt.mat")["pavia_gt"]
    return hsi, gt
def load_paviau():
    hsi = scipy.io.loadmat("../data 1/PaviaU/PaviaU.mat")["paviaU"]
    gt = scipy.io.loadmat("../data 1/PaviaU/PaviaU_gt.mat")["paviaU_gt"]
    return hsi, gt
def load_salinas():
    hsi = scipy.io.loadmat("../data 1/Salinas/Salinas.mat")["salinas"]
    gt = scipy.io.loadmat("../data 1/Salinas/Salinas_gt.mat")["salinas_gt"]
    return hsi, gt

def model_name(prefix):
    return "model_{}".format(prefix)

def build_data(hsi, gt, width=5, num_classes=9):
    # normalize
    cube = np.zeros(hsi.shape)
    for c in range(cube.shape[2]):
        minval = hsi[:,:,c].min()
        maxval = hsi[:,:,c].max()
        cube[:,:,c] = (hsi[:,:,c]-minval)/(maxval-minval)
    cube = (cube*255).astype(np.uint8)
    padding_cube = np.pad(cube, ((width//2,width//2), (width//2,width//2), (0,0)), mode='constant')
    padding_gt = np.pad(gt, ((width//2,width//2), (width//2,width//2)), mode='constant')

    # create data_hsi and data_gts
    data_hsi, data_gts = [], []
    for labelid in range(1, num_classes+1):
        # find the positions for pixels of class 'labelid'
        indexes = np.argwhere(padding_gt == labelid)
        for r, c in indexes:
            subcube = padding_cube[r-width//2:r+width//2+1, c-width//2:c+width//2+1, :]
            subcubes = data_augmentation(subcube)
            data_hsi.append(subcubes)
            data_gts.append([labelid]*len(subcubes))
            # data_hsi += subcubes
            # data_gts += [labelid]*len(subcubes)
    data_hsi = np.array(data_hsi, dtype=np.uint8)
    N, aug, W, H, C = data_hsi.shape
    data_hsi = data_hsi.reshape(N, aug, C, W, H)
    data_gts = np.array(data_gts, dtype=np.uint8)
    return data_hsi, data_gts

def load_data(prefix, width=5, num_classes=9):
    # load data
    if prefix == "multiple":
        data_hsi = []
        data_gts = []
        data_names = ["indian_pines","botswana","ksc","paviac","paviau","salinas"]
        for data in data_names:
            hsi, gt = eval("load_"+data+"()")
            hsis, gts = build_data(hsi, gt, width, num_classes)
            data_hsi.append(hsis)
            data_gts.append(gts)
        data_hsi = np.concatenate(data_hsi, axis=0)
        data_gts = np.concatenate(data_gts, axis=0)
        return data_hsi, data_gts
    else:
        hsi, gt = eval("load_"+prefix+"()")
        hsi = hsi / 65535
        return build_data(hsi, gt, width, num_classes)

def create_train_val_set(data_hsi, data_gts, num_classes=9, samples=15):
    train_hsi, train_gts = [], []
    test_hsi, test_gts = [], []
    data_gts = data_gts - 1 # substract by 1 to convert label from 0
    _, _, C, W, H = data_hsi.shape
    for labelid in range(0, num_classes):
        indexes = np.argwhere(data_gts[:,0] == labelid).reshape(-1)
        np.random.shuffle(indexes)
        train_indexes = indexes[:samples]
        test_indexes = indexes[samples:]
        train_hsi.append(data_hsi[train_indexes].reshape(-1,C,W,H))
        train_gts.append(data_gts[train_indexes].reshape(-1))
        test_hsi.append(data_hsi[test_indexes].reshape(-1,C,W,H))
        test_gts.append(data_gts[test_indexes].reshape(-1))
    train_hsi = np.vstack(train_hsi)
    train_gts = np.concatenate(train_gts)
    test_hsi = np.vstack(test_hsi)
    test_gts = np.concatenate(test_gts)
    val_hsi = test_hsi[:int(len(test_hsi)*0.1)]
    val_gts = test_gts[:int(len(test_gts)*0.1)]
    return train_hsi, train_gts, val_hsi, val_gts, test_hsi, test_gts

def evaluate(model, data_hsi, gts, args):
    batch_size = 256
    n_iters = int(np.ceil(data_hsi.shape[0]/batch_size))
    n_correct = 0
    for iter in range(n_iters):
        batch_hsi = data_hsi[iter*batch_size:(iter+1)*batch_size]
        batch_gts = gts[iter*batch_size:(iter+1)*batch_size]
        scores = model(batch_hsi)
        predict_labels = scores.max(dim=1)[1]
        correct = predict_labels == batch_gts
        n_correct += correct.sum()
    acc = float(n_correct)*100/data_hsi.shape[0]
    return acc

def train(model, data_hsi, gts, test_hsi, test_gts, args):
    optimizer = torch.optim.Adam(filter(lambda p : p.requires_grad, model.parameters()), lr=args.learning_rate)

    batch_size, epochs = args.batch_size, args.epochs
    n_iters = int(np.ceil(data_hsi.shape[0]/batch_size))
    avg_time = 0.0
    total_steps = 0
    best_acc = -1
    indexes = np.arange(data_hsi.shape[0])
    for epoch in range(epochs):
        print("Epoch {}".format(epoch+1))
        np.random.shuffle(indexes)
        data_hsi = data_hsi[indexes]
        gts = gts[indexes]
        for iter in range(n_iters):
            batch_hsi = data_hsi[iter*batch_size:(iter+1)*batch_size]
            batch_gts = gts[iter*batch_size:(iter+1)*batch_size]
            start_time = time.time()

            optimizer.zero_grad()
            loss = model.loss(batch_hsi, batch_gts)
            loss.backward()
            optimizer.step()

            avg_time = (avg_time * total_steps + time.time() - start_time) / (total_steps + 1)
            if iter % args.print_every == 0:
                print("Iter:", '%03d'%iter, "train_loss=", loss.item(), "avg_time=", avg_time)

            if iter % args.validate_iter == 0:
                acc = evaluate(model, test_hsi, test_gts, args)
                print("Val acc: ", acc, "%")
                if acc > best_acc:
                    best_acc = acc
                    torch.save(model, model_name(args.prefix))
            total_steps += 1
    # print("Best acc: ", best_acc, "%")

def run(data_hsi, gts, args):
    input_size = args.input_size
    num_channels = data_hsi.shape[2]

    train_hsi, train_gts, val_hsi, val_gts, test_hsi, test_gts = create_train_val_set(data_hsi, data_gts, num_classes=args.num_classes, samples=args.samples)
    train_hsi, val_hsi, test_hsi = torch.FloatTensor(train_hsi), torch.FloatTensor(val_hsi), torch.FloatTensor(test_hsi)
    train_gts, val_gts, test_gts = torch.LongTensor(train_gts), torch.LongTensor(val_gts), torch.LongTensor(test_gts)

    model = Model(input_size=input_size, num_channels=num_channels, num_classes=args.num_classes+1)
    if args.cuda:
        train_hsi, train_gts = train_hsi.cuda(), train_gts.cuda()
        test_hsi, test_gts = test_hsi.cuda(), test_gts.cuda()
        val_hsi, val_gts = val_hsi.cuda(), val_gts.cuda()
        model = model.cuda()

    # train
    print("======== Training =========")
    train(model, train_hsi, train_gts, val_hsi, val_gts, args)
    # torch.cuda.empty_cache()
    # evaluate
    print("========= Test set ========")
    # if args.cuda:
    #     test_hsi, test_gts = test_hsi.cuda(), test_gts.cuda()
    best_model = torch.load(model_name(args.prefix))
    acc = evaluate(best_model, test_hsi, test_gts, args)
    torch.cuda.empty_cache()
    print("Accuracy: {0:.2f}%".format(acc))

if __name__ == '__main__':
    args = parse_args()
    print(args)
    data_hsi, data_gts = load_data(args.prefix, width=args.input_size, num_classes=args.num_classes)
    run(data_hsi, data_gts, args)
